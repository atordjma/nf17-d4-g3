**Ressource**

    Type : {livre, film, musique};
    Code : integer #unique;
    Titre : string;
    Liste_contributeurs : ??? ;
    Date_apparition : date
    Editeur : string ;
    Genre : string ;
    Classification : string ;
    Prix : integer ;
    
*Classe mère de : Livre, Musique, Film*

**Exemplaire**

    Document : integer (=>Ressource.Code) ;
    Disponibilité : {Emprunté, Réservé, Libre} ;
    Etat : {neuf, bon, abîmé, perdu} ;
    Num_exemplaire : integer ; 
    
*(Document, Num_exemplaire) #primary key*

    
**Contributeur**

    Nom : string ;
    Prénom : string ;
    Date_naissance : date
    Nationalité : string ;
    
*clé ?*
    
**Livre**

*Hérite de Ressource*

    Auteur : string #doit exister dans Contributeur ;
    ISBN : string ;
    Résumé : string ;
    Langue : string ;
    
**Musique**

*Hérite de Ressource*

    Compositeur : string #doit exister dans Contributeur ;
    Interprète : string #doit exister dans Contributeur ;
    Longueur : durée ;
    
**Film**

*Hérite de Ressource*

    Réalisateur : string #doit exister dans Contributeur ;
    Acteur : string #doit exister dans Contributeur ;
    Langue : string ;
    Longueur : durée ;
    
**Personnel**

    login_admin : string ;
    pwd_admin : string ;
    Nom : string ;
    Prénom : string ;
    Adresse : string ;
    Mail : string ;
    
*(login_admin, pwd_admin) #primary key*
    
**Adhérent**

    login : string ;
    pwd : string ;
    Nom : string ;
    Prénom : string ;
    Num_carte : integer #unique;
    Mail : string ;
    Num_tél : string ;
    Adhésion : {passée, actuelle} ;
    Nb_emprunt : integer ;
    Nb_sanctions : integer ;
    
*(login, pwd) #primary key*
    
**Emprunt**

    Document : integer (=>Ressource.Code) ;
    login_adhérent : string (=>Adhérent.login) ;
    pwd_adhérent : string (=>Adhérent.pwd) ;
    Date_emprunt : date ;
    Durée : durée ;
    
*(Document, login_adhérent, pwd_adhérent) #primary key*
    
**Retour**

    Document : integer (=>Ressource.Code) ;
    login_adhérent : string (=>Adhérent.login) ;
    pwd_adhérent : string (=>Adhérent.pwd) ;
    Date_retour : date ;
    Retard : durée ;
    
*(Document, login_adhérent, pwd_adhérent) #primary key*
    
**Sanction**

    Type : {Retard, remboursement} ;
    login_adhérent : string (=>Adhérent.login) ;
    pwd_adhérent : string (=>Adhérent.pwd) ;
    Document : integer (=>Ressource.Code) ;
    Début_suspension : date ;
    Durée_suspension : durée ;
    
*(Document, login_adhérent, pwd_adhérent) #primary key*
*Durée_suspension n'existe que si Type = Retard*

**Réservation**

    login_adhérent : string (=>Adhérent.login) ;
    pwd_adhérent : string (=>Adhérent.pwd) ;
    Document : integer (=>Ressource.Code) ;
    
*(Document, login_adhérent, pwd_adhérent) #primary key*

**Contraintes supplémentaires**

    (Personnel.login_admin,Personel.pwd_admin) != (Adhérent.login, Adhérent.pwd) pour tous les couples dans Personnel et Adhérent
    